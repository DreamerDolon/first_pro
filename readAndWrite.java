import java.io.*;
import java.util.*;

public class readAndWrite {

	public static void main(String agrs[]) {

		int isFound = 0;
		File newFile = new File("F://temp.txt");
		String line = null, allLowerCase = null;
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter any word to be searched:");

		String wordToBeSearched = null;
		// wordToBeSearched = scan.nextLine();
		wordToBeSearched = scan.nextLine().toLowerCase();

		try {

			FileReader readFile = new FileReader(newFile);
			BufferedReader buffRead = new BufferedReader(readFile);
			FileWriter writeFile = new FileWriter("F://output.txt");

			while ((line = buffRead.readLine()) != null) {
				allLowerCase = line.toLowerCase();
				if (allLowerCase.matches("(.*)" + " " + wordToBeSearched + " " + "(.*)")
						|| allLowerCase.matches("(.*)" + " " + wordToBeSearched + "" + "(.*)")
						|| allLowerCase.matches("(.*)" + "" + wordToBeSearched + " " + "(.*)")) {
					writeFile.append(line + '\n');
					System.out.println(line);
					isFound=1;
					// System.out.println(line+"\n");
				}
			}
			

			  if (isFound ==0) {
				  System.out.println("No matches found.");
				  writeFile.append("No matches found.");
			  }
			  
				writeFile.close();
				buffRead.close();

			
		} catch (FileNotFoundException e) {
			System.out
					.println("File " + newFile.getAbsolutePath() + " could not be found. Please check the location...");
		}

		catch (IOException ioe) {

			System.out.println("Exeption while reading the file " + ioe);

		}

	}

}
